import 'package:flutter_definitivo/posicionamiento.dart';

class Analizador {
  List<Posicionamiento> _obtenerPosicionamientos({required String palabra}) {
    return palabra
        .split('')
        .asMap()
        .entries
        .map((e) => Posicionamiento(posicion: e.key, letra: e.value))
        .toList();
  }

  List<Posicionamiento> obtenerCoincidenciasExactas(
      {required String arquetipo, required String intento}) {
    var posicionamientosArquetipo =
        _obtenerPosicionamientos(palabra: arquetipo);
    var posicionamientosIntento = _obtenerPosicionamientos(palabra: intento);

    return posicionamientosArquetipo
        .toSet()
        .intersection(posicionamientosIntento.toSet())
        .toList();
  }

  List<Posicionamiento> obtenerCoincidenciasNoExactas(
      {required String arquetipo, required String intento}) {
    var posicionamientosArquetipo =
        _obtenerPosicionamientos(palabra: arquetipo);
    var posicionamientosIntento = _obtenerPosicionamientos(palabra: intento);
    var coincidenciasExactas =
        obtenerCoincidenciasExactas(arquetipo: arquetipo, intento: intento);

    List<Posicionamiento> coincidenciasNoExactas = _comparaLetrasNoExactas(
        posicionamientosArquetipo,
        posicionamientosIntento,
        coincidenciasExactas);
    return coincidenciasNoExactas;
  }

  List<Posicionamiento> _comparaLetrasNoExactas(
      List<Posicionamiento> posArquetipo,
      List<Posicionamiento> posIntento,
      List<Posicionamiento> exactas) {
    List<Posicionamiento> coincidenciasNoExactas = [];
    for (var elemento in posArquetipo) {
      coincidenciasNoExactas.addAll(posIntento.where((e) {
        return e.letra == elemento.letra &&
            !exactas.contains(e) &&
            !coincidenciasNoExactas.contains(e);
      }));
    }
    coincidenciasNoExactas =
        _limpiaDuplicados(coincidenciasNoExactas, posArquetipo, exactas);
    return coincidenciasNoExactas;
  }

  List<Posicionamiento> _limpiaDuplicados(List<Posicionamiento> noExactas,
      List<Posicionamiento> posArquetipo, List<Posicionamiento> exactas) {
    List<String> letrasYaVistas = [];
    for (var i = 0; i < posArquetipo.length; i++) {
      String letraActual = posArquetipo[i].letra;
      if (!letrasYaVistas.contains(letraActual)) {
        letrasYaVistas.add(letraActual);
        int vecesRepetida = _obtenerRepeticiones(posArquetipo, letraActual);
        int extrasNoExactas = _obtenerRepeticiones(noExactas, letraActual);
        int extrasExactas = _obtenerRepeticiones(exactas, letraActual);
        while ((extrasNoExactas + extrasExactas) > vecesRepetida) {
          int largoTotalNoExactas = noExactas.length - 1;
          if (noExactas[largoTotalNoExactas].letra == letraActual) {
            noExactas.removeAt(largoTotalNoExactas);
          }
          largoTotalNoExactas = largoTotalNoExactas - 1;
          extrasNoExactas = _obtenerRepeticiones(noExactas, letraActual);
        }
      }
    }
    return noExactas;
  }

  int _obtenerRepeticiones(List<Posicionamiento> lista, String letraActual) {
    int repeticiones = 0;
    for (var letra in lista) {
      if (letra.letra == letraActual) repeticiones++;
    }
    return repeticiones;
  }

  List<Posicionamiento> obtenerLetrasNoUsadas(
      String arquetipo, String intento) {
    var posicionamientosIntento = _obtenerPosicionamientos(palabra: intento);
    List<Posicionamiento> letrasNoUsadas = [];
    List<Posicionamiento> exactas =
        obtenerCoincidenciasExactas(arquetipo: arquetipo, intento: intento);
    List<Posicionamiento> noExactas =
        obtenerCoincidenciasNoExactas(arquetipo: arquetipo, intento: intento);
    var elementosCategorizados = exactas + noExactas;
    letrasNoUsadas.addAll(posicionamientosIntento.where((e) {
      return !elementosCategorizados.contains(e);
    }));
    return letrasNoUsadas;
  }
}
