import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_definitivo/evento.dart';
import 'package:flutter_definitivo/estado.dart';
import 'package:flutter_definitivo/analizador.dart';
import 'package:flutter_definitivo/main.dart';
import 'package:flutter_definitivo/palabra.dart';
import 'package:flutter_definitivo/posicionamiento.dart';

import 'ayuda.dart';

var analizador = Analizador();

class MiBloc extends Bloc<Evento, Estado> {
  List<Posicionamiento> listaExactas = [];
  List<Posicionamiento> listaNoExactas = [];
  List<Posicionamiento> listaIntexistente = [];
  List<String> jugadas = [];
  Palabra palabraActual = Palabra(letras: List.generate(5, (index) => ""));
  String arquetipo = "";
  List<Palabra> listaPalabras =
      List.generate(6, (_) => Palabra(letras: List.generate(5, (_) => "")));
  MiBloc() : super(Estado()) {
    on<EventoEnviaPalabra>((EventoEnviaPalabra event, emit) {
      String intento = event.palabra.letras.join();
      palabraActual = event.palabra;
      arquetipo = event.arquetipo;
      listaPalabras = event.almacenDeIntentos;

      var exactas = analizador.obtenerCoincidenciasExactas(
          arquetipo: event.arquetipo, intento: intento);
      var noExactas = analizador.obtenerCoincidenciasNoExactas(
          arquetipo: event.arquetipo, intento: intento);
      var inexistentes =
          analizador.obtenerLetrasNoUsadas(event.arquetipo, intento);
      for (var posicionamiento in exactas) {
        listaExactas.add(posicionamiento);
      }
      for (var posicionamiento in noExactas) {
        listaNoExactas.add(posicionamiento);
      }
      for (var posicionamiento in inexistentes) {
        listaIntexistente.add(posicionamiento);
      }

      jugadas.add(intento);

      listaExactas = listaExactas.toSet().toList();
      listaNoExactas = listaNoExactas.toSet().toList();

      emit(EstadoJugando(
          correctas: listaExactas,
          enArquetipo: listaNoExactas,
          inexistentes: listaIntexistente,
          jugadasHechas: jugadas,
          palabraAEscribir: event.palabra,
          listaPalabras: listaPalabras,
          arquetipo: event.arquetipo));
      if (exactas.length == 5 || jugadas.length == 6) {
        emit(EstadoJuegoTerminado(
            correctas: listaExactas,
            enArquetipo: listaNoExactas,
            inexistentes: listaIntexistente,
            jugadasHechas: jugadas,
            palabraAEscribir: event.palabra,
            listaPalabras: listaPalabras,
            arquetipo: event.arquetipo));
      }
    });
    on<EventoReinicia>((EventoReinicia event, emit) {
      listaExactas = [];
      listaNoExactas = [];
      jugadas = [];
      listaIntexistente = [];
      List<Palabra> listaPalabras =
          List.generate(6, (_) => Palabra(letras: List.generate(5, (_) => "")));
      palabraActual = Palabra(letras: List.generate(5, (index) => ""));
      emit(EstadoJugando(
          correctas: listaExactas,
          enArquetipo: listaNoExactas,
          inexistentes: listaIntexistente,
          jugadasHechas: jugadas,
          palabraAEscribir: event.palabra,
          listaPalabras: listaPalabras,
          arquetipo: event.arquetipo));
    });
    on<EventoAbreAyuda>((EventoAbreAyuda event, emit) {
      emit(EstadoEnAyuda());
    });
    on<EventoCierraAyuda>((EventoCierraAyuda event, emit) {
      emit(EstadoJugando(
          jugadasHechas: jugadas,
          correctas: listaExactas,
          enArquetipo: listaNoExactas,
          inexistentes: listaIntexistente,
          palabraAEscribir: palabraActual,
          listaPalabras: listaPalabras,
          arquetipo: arquetipo));
    });
  }
}
