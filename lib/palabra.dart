import 'package:equatable/equatable.dart';

class Palabra extends Equatable {
  const Palabra({required this.letras});
  final List<String> letras;

  @override
  List<Object?> get props => [letras];

  String get wordString => letras.map((e) => e).join();

  void escribeLetra(String val) {
    final indiceActual = letras.indexWhere((e) => e.isEmpty);
    if (indiceActual != -1) {
      letras[indiceActual] = val;
    }
  }

  void borraLetra() {
    final posicionLetraNoVacia = letras.lastIndexWhere((e) => e.isNotEmpty);

    if (posicionLetraNoVacia != -1) {
      letras[posicionLetraNoVacia] = "";
    }
  }

  int obtenLargoPalabra() {
    return letras.lastIndexWhere((element) => element.isNotEmpty);
  }
}
