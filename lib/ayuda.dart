import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_definitivo/bloc.dart';
import 'package:flutter_definitivo/evento.dart';

class Ayuda extends StatelessWidget {
  const Ayuda({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(30.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "Adivina la palabra oculta en seis intentos.",
            textAlign: TextAlign.justify,
          ),
          const Text(
            "Cada intento debe ser una palabra válida de 5 letras.",
            textAlign: TextAlign.justify,
          ),
          const Text(
            "Después de cada intento el color de las letras cambia para mostrar qué tan cerca estás de acertar la palabra.",
            textAlign: TextAlign.justify,
          ),
          const Text("Ejemplo", style: TextStyle(fontWeight: FontWeight.bold)),
          Image.asset("assets/1.JPG"),
          const Text(
              "La letra R está en la palabra y en la posición correcta."),
          Image.asset("assets/2.JPG"),
          const Text(
              "La letra C está en la palabra pero en la posición incorrecta."),
          Image.asset("assets/3.JPG"),
          const Text("La letra O no está en la palabra."),
          const Text(
              "Puede haber letras repetidas y en ese caso las pistas son independientes para cada letra y tienen prioridad."),
          ElevatedButton(
              onPressed: () {
                var bloc = context.read<MiBloc>();
                bloc.add(EventoCierraAyuda());
              },
              child: const Text("salir de ayuda"))
        ],
      ),
    );
  }
}
