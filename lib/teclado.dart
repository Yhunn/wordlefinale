import 'package:flutter/material.dart';
import 'package:flutter_definitivo/bloc.dart';
import 'package:flutter_definitivo/colores.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_definitivo/estado.dart';
import 'package:flutter_definitivo/posicionamiento.dart';

const _teclas = [
  ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
  ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'],
  ['ENTER', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 'DEL'],
];

enum EstadoTecla { verde, amarillo, gris, negro, delete, enter, noEsta }

class WidgetTeclado extends StatelessWidget {
  const WidgetTeclado({
    Key? key,
    required this.alPresionarLetra,
    required this.alPresionarEnter,
    required this.alPresionarDelete,
    required this.letras,
  }) : super(key: key);

  final void Function(String) alPresionarLetra;
  final VoidCallback alPresionarEnter;
  final VoidCallback alPresionarDelete;
  final Set<String> letras;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: _teclas
          .map((filaTeclas) => Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: filaTeclas.map((letra) {
                  if (letra == "DEL") {
                    return Tecla.delete(
                      accion: alPresionarDelete,
                    );
                  } else if (letra == "ENTER") {
                    return Tecla.enter(
                      accion: alPresionarEnter,
                    );
                  }

                  var estado = context.watch<MiBloc>().state;
                  if (estado is EstadoEnWordle) {
                    List<Posicionamiento> exactas = estado.correctas;
                    List<Posicionamiento> noExactas = estado.enArquetipo;
                    List<Posicionamiento> noEsta = estado.inexistentes;
                    List<String> aPintarVerde = [];
                    List<String> aPintarAmarillo = [];
                    List<String> aPintarGris = [];
                    for (var pos in exactas) {
                      aPintarVerde.add(pos.letra);
                    }
                    for (var pos in noExactas) {
                      aPintarAmarillo.add(pos.letra);
                    }
                    for (var pos in noEsta) {
                      aPintarGris.add(pos.letra);
                    }
                    if (aPintarVerde.contains(letra)) {
                      return Tecla(
                        accion: () => alPresionarLetra(letra),
                        letra: letra,
                        estadoactual: EstadoTecla.verde,
                      );
                    }
                    if (aPintarAmarillo.contains(letra)) {
                      return Tecla(
                        accion: () => alPresionarLetra(letra),
                        letra: letra,
                        estadoactual: EstadoTecla.amarillo,
                      );
                    }
                    if (aPintarGris.contains(letra)) {
                      return Tecla(
                        accion: () => alPresionarLetra(letra),
                        letra: letra,
                        estadoactual: EstadoTecla.noEsta,
                      );
                    }
                  }

                  return Tecla(
                      accion: () => alPresionarLetra(letra), letra: letra);
                }).toList(),
              ))
          .toList(),
    );
  }
}

class Tecla extends StatelessWidget {
  const Tecla({
    Key? key,
    this.alto = 48,
    this.largo = 30,
    this.estadoactual = EstadoTecla.gris,
    required this.accion,
    required this.letra,
  }) : super(key: key);

  factory Tecla.delete({required VoidCallback accion}) => Tecla(
        accion: accion,
        //colorDeFondo: Color.fromARGB(255, 240, 119, 119),
        estadoactual: EstadoTecla.delete,
        letra: "DEL",
        largo: 56,
      );
  factory Tecla.enter({required VoidCallback accion}) => Tecla(
        accion: accion,
        //colorDeFondo: Color.fromARGB(255, 146, 243, 227),
        estadoactual: EstadoTecla.enter,
        letra: "ENTER",
        largo: 56,
      );

  final double alto;
  final double largo;
  final VoidCallback accion;
  final EstadoTecla estadoactual;
  final String letra;

  Color setColor() {
    switch (estadoactual) {
      case EstadoTecla.verde:
        return colorCorrecto;
      case EstadoTecla.amarillo:
        return colorNoExacto;
      case EstadoTecla.gris:
        return const Color.fromARGB(255, 194, 194, 194);
      case EstadoTecla.negro:
        return colorFalloTecla;
      case EstadoTecla.enter:
        return const Color.fromARGB(255, 146, 243, 227);
      case EstadoTecla.delete:
        return const Color.fromARGB(255, 240, 119, 119);
      case EstadoTecla.noEsta:
        return const Color.fromARGB(255, 78, 78, 78);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 2.0),
      child: Material(
        color: setColor(),
        borderRadius: BorderRadius.circular(4),
        child: InkWell(
          onTap: accion,
          child: Container(
              height: alto,
              width: largo,
              alignment: Alignment.center,
              child: Text(
                letra,
                style:
                    const TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
              )),
        ),
      ),
    );
  }
}
