class Posicionamiento {
  final int posicion;
  final String letra;

  const Posicionamiento({required this.posicion, required this.letra});
  @override
  bool operator ==(other) =>
      other is Posicionamiento &&
      posicion == other.posicion &&
      letra == other.letra;
  @override
  int get hashCode => posicion.hashCode ^ letra.hashCode;
  @override
  toString() => 'Posicionamiento($posicion:$letra)';
}
