import 'package:flutter/material.dart';

const Color colorCorrecto = Color.fromARGB(190, 138, 224, 143);
const Color colorNoExacto = Color.fromARGB(230, 206, 218, 105);
const Color colorFalloCasilla = Colors.grey;
const Color colorFalloTecla = Colors.black26;
