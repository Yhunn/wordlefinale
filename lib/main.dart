import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_definitivo/ayuda.dart';
import 'package:flutter_definitivo/bloc.dart';
import 'package:flutter_definitivo/estado.dart';
import 'package:flutter_definitivo/evento.dart';
import 'package:flutter_definitivo/palabra.dart';
import 'package:flutter_definitivo/tablero.dart';
import 'package:flutter_definitivo/teclado.dart';
import 'package:flutter_definitivo/colores.dart';
import 'package:flutter_definitivo/analizador.dart';

void main() {
  runApp(const AplicacionBloc());
}

class AplicacionBloc extends StatelessWidget {
  const AplicacionBloc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MiBloc(),
      child: const Aplicacion(),
    );
  }
}

class Aplicacion extends StatelessWidget {
  const Aplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: const Text('Wordle Bloc'),
        actions: [
          IconButton(
            onPressed: () {
              var bloc = context.read<MiBloc>();
              bloc.add(EventoAbreAyuda());
            },
            icon: const Icon(Icons.help_outline_outlined),
            color: Colors.black,
          ),
        ],
      ),
      body: SelectorDeVista(),
    ));
  }
}

class SelectorDeVista extends StatelessWidget {
  const SelectorDeVista({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var estado = context.watch<MiBloc>().state;
    if (estado is EstadoEnAyuda) {
      return Ayuda();
    }
    return MiWidgetEstado();
  }
}

class MiWidgetEstado extends StatefulWidget {
  const MiWidgetEstado({Key? key}) : super(key: key);

  @override
  State<MiWidgetEstado> createState() => _MiWidgetEstadoState();
}

class _MiWidgetEstadoState extends State<MiWidgetEstado> {
  List<Palabra> _almacenIntentos =
      List.generate(6, (_) => Palabra(letras: List.generate(5, (_) => "")));

  final Set<String> _letrasDelTeclado = {};

  int indiceDePalabra = 0;

  Palabra? get intentoActual => indiceDePalabra < _almacenIntentos.length
      ? _almacenIntentos[indiceDePalabra]
      : null;

  final String arquetipo = "HELLO";

  int aciertoMayor = 0;
  int posicionMayor = 0;

  @override
  Widget build(BuildContext context) {
    var estado = context.watch<MiBloc>().state;
    if (estado is EstadoJugando) {
      setState(() {
        _almacenIntentos = estado.listaPalabras;
        indiceDePalabra = estado.jugadasHechas.length;
      });
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        WidgetTablero(intentos: _almacenIntentos),
        const SizedBox(
          height: 15,
        ),
        WidgetTeclado(
          alPresionarDelete: checaEstadoDelete(context),
          alPresionarEnter: checaEstadoEnter(context),
          alPresionarLetra: checaEstadoEscribe(context),
          letras: _letrasDelTeclado,
        ),
      ],
    );
  }

  void _enTeclaPresionada(String valor) {
    setState(() => intentoActual?.escribeLetra(valor));
  }

  void _enPresionaBorrar() {
    setState(() => intentoActual?.borraLetra());
  }

  void _enPresionaEnter() {
    if (intentoActual?.obtenLargoPalabra() == 4) {
      var bloc = context.read<MiBloc>();
      bloc.add(EventoEnviaPalabra(
          _almacenIntentos[indiceDePalabra], arquetipo, _almacenIntentos));
      indiceDePalabra++;
    }
  }

  VoidCallback checaEstadoEnter(BuildContext contexto) {
    generaMensajePorTurno();
    var estado = context.watch<MiBloc>().state;
    if (estado is! EstadoJuegoTerminado) {
      return _enPresionaEnter;
    }

    mensajeGanaOPierde();
    return () {};
  }

  VoidCallback checaEstadoDelete(BuildContext contexto) {
    var estado = context.watch<MiBloc>().state;
    if (estado is! EstadoJuegoTerminado) {
      return _enPresionaBorrar;
    }
    return () {};
  }

  void Function(String) checaEstadoEscribe(BuildContext contexto) {
    var estado = context.watch<MiBloc>().state;
    if (estado is EstadoJugando) {
      setState(() {
        _almacenIntentos = estado.listaPalabras;
        indiceDePalabra = estado.jugadasHechas.length;
      });
    }
    if (estado is! EstadoJuegoTerminado) {
      return _enTeclaPresionada;
    }
    return (string) {};
  }

  void mensajeGanaOPierde() {
    var estado = context.watch<MiBloc>().state;
    if (estado is EstadoJuegoTerminado) {
      Color colorDeSalida = Colors.red;
      String mesajeDeSalida = "La respuesta correcta era: " + estado.arquetipo;
      var exactas = Analizador().obtenerCoincidenciasExactas(
          arquetipo: arquetipo,
          intento: estado.jugadasHechas[indiceDePalabra - 1]);
      if (exactas.length == 5) {
        colorDeSalida = colorCorrecto;
        mesajeDeSalida = "FELICIDADES HAS GANADO";
      }

      SnackBar snackBar = SnackBar(
        dismissDirection: DismissDirection.down,
        duration: const Duration(seconds: 5),
        backgroundColor: colorDeSalida,
        content:
            Text(mesajeDeSalida, style: const TextStyle(color: Colors.white)),
        action: SnackBarAction(
          onPressed: reiniciarJuego,
          label: "¿Volver a Jugar?",
          textColor: Colors.white,
        ),
      );
      Future.delayed(Duration.zero, () {
        Scaffold.of(context).showSnackBar(snackBar);
      });
    }
  }

  void generaMensajePorTurno() {
    var estado = context.watch<MiBloc>().state;
    if (estado is EstadoJugando) {
      if (intentoActual!.letras[0] == "" && indiceDePalabra != 0) {
        List<String> mensajesNegativos = [
          'Lo haces super mal colega',
          'Vuelve a primaria compañero',
          'Casi... pero no...',
          'chale que pena, más la que me das',
        ];
        List<String> mensajesPositivos = [
          'Buen trabajo, ahora no la arruines',
          'Hasta parece que estudiaste ehh',
          'Buena! ahora otra que no sea suerte!',
          'Sigue así!!!',
        ];

        String mensajePorTurno =
            mensajesNegativos[Random().nextInt(mensajesNegativos.length)];
        Color colorPorTurno = Colors.red;

        for (var i = 0; i < estado.jugadasHechas.length; i++) {
          var exactasEstaJugada = Analizador().obtenerCoincidenciasExactas(
              arquetipo: arquetipo, intento: estado.jugadasHechas[i]);
          var noExactasEstaJugada = Analizador().obtenerCoincidenciasNoExactas(
              arquetipo: arquetipo, intento: estado.jugadasHechas[i]);
          int aciertosEstaJugada =
              exactasEstaJugada.length + noExactasEstaJugada.length;
          if (i == indiceDePalabra - 1) {
            if (aciertosEstaJugada > aciertoMayor) {
              aciertoMayor = aciertosEstaJugada;
              posicionMayor = i;
              mensajePorTurno =
                  mensajesPositivos[Random().nextInt(mensajesPositivos.length)];
              colorPorTurno = colorCorrecto;
            }
          }
        }

        SnackBar snackBar = SnackBar(
          dismissDirection: DismissDirection.down,
          duration: const Duration(seconds: 5),
          backgroundColor: colorPorTurno,
          content: Text(mensajePorTurno,
              style: const TextStyle(color: Colors.white)),
          action: SnackBarAction(
            onPressed: () {},
            label: "Ok",
            textColor: Colors.white,
          ),
        );
        Future.delayed(Duration.zero, () {
          Scaffold.of(context).showSnackBar(snackBar);
        });
      }
    }
  }

  void reiniciarJuego() {
    setState(() {
      _almacenIntentos =
          List.generate(6, (_) => Palabra(letras: List.generate(5, (_) => "")));
      indiceDePalabra = 0;
      aciertoMayor = 0;
      posicionMayor = 0;
    });
    var bloc = context.read<MiBloc>();
    bloc.add(EventoReinicia(_almacenIntentos[indiceDePalabra], arquetipo));
  }
}
