import 'package:flutter_definitivo/palabra.dart';
import 'package:flutter_definitivo/posicionamiento.dart';

class Estado {}

class EstadoEnWordle extends Estado {
  List<String> jugadasHechas;
  List<Posicionamiento> correctas;
  List<Posicionamiento> enArquetipo;
  List<Posicionamiento> inexistentes;
  Palabra palabraAEscribir;
  String arquetipo;
  List<Palabra> listaPalabras;
  EstadoEnWordle(
      {required this.jugadasHechas,
      required this.correctas,
      required this.enArquetipo,
      required this.inexistentes,
      required this.palabraAEscribir,
      required this.listaPalabras,
      required this.arquetipo});
}

class EstadoJugando extends EstadoEnWordle {
  EstadoJugando(
      {required List<String> jugadasHechas,
      required List<Posicionamiento> correctas,
      required List<Posicionamiento> enArquetipo,
      required List<Posicionamiento> inexistentes,
      required Palabra palabraAEscribir,
      required List<Palabra> listaPalabras,
      required String arquetipo})
      : super(
            jugadasHechas: jugadasHechas,
            correctas: correctas,
            enArquetipo: enArquetipo,
            inexistentes: inexistentes,
            palabraAEscribir: palabraAEscribir,
            listaPalabras: listaPalabras,
            arquetipo: arquetipo);
}

class EstadoJuegoTerminado extends EstadoEnWordle {
  EstadoJuegoTerminado(
      {required List<String> jugadasHechas,
      required List<Posicionamiento> correctas,
      required List<Posicionamiento> enArquetipo,
      required Palabra palabraAEscribir,
      required List<Posicionamiento> inexistentes,
      required List<Palabra> listaPalabras,
      required String arquetipo})
      : super(
            jugadasHechas: jugadasHechas,
            correctas: correctas,
            enArquetipo: enArquetipo,
            inexistentes: inexistentes,
            palabraAEscribir: palabraAEscribir,
            listaPalabras: listaPalabras,
            arquetipo: arquetipo);
}

class EstadoEnAyuda extends Estado {}
