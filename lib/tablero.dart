import 'package:flutter/material.dart';
import 'package:flutter_definitivo/palabra.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_definitivo/bloc.dart';
import 'package:flutter_definitivo/estado.dart';
import 'package:flutter_definitivo/colores.dart';
import 'package:flutter_definitivo/analizador.dart';

enum EstadoCasilla { verde, amarillo, inexistente, inicial }

class WidgetTablero extends StatelessWidget {
  final List<Palabra> intentos;
  const WidgetTablero({required this.intentos, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: intentos
          .asMap()
          .entries
          .map(
            (jugadaIntento) => Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: jugadaIntento.value.letras.asMap().entries.map((letra) {
                var estado = context.watch<MiBloc>().state;
                if (estado is EstadoEnWordle) {
                  List<String> jugadas = estado.jugadasHechas;
                  for (var i = 0; i < jugadas.length; i++) {
                    List<int> aPintarVerde = [];
                    List<int> aPintarAmarillo = [];
                    List<int> aPintarGris = [];
                    var exactasEsteIntento = Analizador()
                        .obtenerCoincidenciasExactas(
                            arquetipo: estado.arquetipo, intento: jugadas[i]);
                    var noExactasEsteIntento = Analizador()
                        .obtenerCoincidenciasNoExactas(
                            arquetipo: estado.arquetipo, intento: jugadas[i]);
                    var inexistentesEsteIntento = Analizador()
                        .obtenerLetrasNoUsadas(estado.arquetipo, jugadas[i]);

                    for (var pos in exactasEsteIntento) {
                      aPintarVerde.add(pos.posicion);
                    }
                    for (var pos in noExactasEsteIntento) {
                      aPintarAmarillo.add(pos.posicion);
                    }
                    for (var pos in inexistentesEsteIntento) {
                      aPintarGris.add(pos.posicion);
                    }

                    if (aPintarVerde.contains(letra.key) &&
                        i == jugadaIntento.key) {
                      return CasillaTablero(
                        letra: letra.value,
                        colorCasilla: EstadoCasilla.verde,
                      );
                    }
                    if (aPintarAmarillo.contains(letra.key) &&
                        i == jugadaIntento.key) {
                      return CasillaTablero(
                        letra: letra.value,
                        colorCasilla: EstadoCasilla.amarillo,
                      );
                    }
                    if (aPintarGris.contains(letra.key) &&
                        i == jugadaIntento.key) {
                      return CasillaTablero(
                        letra: letra.value,
                        colorCasilla: EstadoCasilla.inexistente,
                      );
                    }
                  }
                }
                return CasillaTablero(letra: letra.value);
              }).toList(),
            ),
          )
          .toList(),
    );
  }
}

class CasillaTablero extends StatelessWidget {
  const CasillaTablero(
      {this.colorCasilla = EstadoCasilla.inicial,
      required this.letra,
      Key? key})
      : super(key: key);

  final String letra;
  final EstadoCasilla colorCasilla;

  Color setColor() {
    switch (colorCasilla) {
      case EstadoCasilla.verde:
        return colorCorrecto;
      case EstadoCasilla.amarillo:
        return colorNoExacto;
      case EstadoCasilla.inexistente:
        return const Color.fromARGB(255, 87, 87, 87);
      case EstadoCasilla.inicial:
        return Colors.transparent;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(4),
      height: 48,
      width: 48,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: setColor(),
        border: Border.all(color: const Color.fromARGB(255, 107, 105, 105)),
        borderRadius: BorderRadius.circular(4),
      ),
      child: Text(
        letra,
        style: const TextStyle(
          fontSize: 32,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
