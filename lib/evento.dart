import 'package:flutter/cupertino.dart';
import 'package:flutter_definitivo/palabra.dart';

class Evento {}

class EventoEnviaPalabra extends Evento {
  final Palabra palabra;
  final String arquetipo;
  final List<Palabra> almacenDeIntentos;
  EventoEnviaPalabra(this.palabra, this.arquetipo, this.almacenDeIntentos);
}

class EventoAbreAyuda extends Evento {}

class EventoCierraAyuda extends Evento {}

class EventoReinicia extends Evento {
  final Palabra palabra;
  final String arquetipo;
  EventoReinicia(this.palabra, this.arquetipo);
}
